<?php
    require_once '../include/DbHandler.php';
    require '.././libs/Slim/Slim.php';

    \Slim\Slim::registerAutoloader();
    
    $app = new \Slim\Slim();
    
    /**
     * Função para verificar os paramêtros recebidos
     */
    function verifyRequiredParams($campos_obrigadorios) {
        $error = false;
        $error_campos = "";
        $param_enviado = array();
        $param_enviado = $_REQUEST;
        
        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $app = \Slim\Slim::getInstance();            
            parse_str($app->request()->getBody(), $param_enviado);                 
        }
        
        foreach ($campos_obrigadorios as $campo) {
            if (!isset($param_enviado[$campo]) || strlen(trim($param_enviado[$campo])) <= 0) {
                $error = true;
                $error_campos .= $campo . ', ';
            }
        }
        
        if ($error) {
            // retorna erro e para o app
            $response = array();
            $app = \Slim\Slim::getInstance();
            $response["error"] = true;
            $response["message"] = 'O(s) campo(s) ' . substr($error_campos, 0, -2) . ' está(ão) faltando ou está(ão) vazio(s)';
            echoResponse(400, $response);
            $app->stop();
        }
    }
    
    /**
     * Resposta em json para client
     * @param String $status_code Código HTTP a ser retornado
     * @param Int $response Resposta Json
     */ 
    function echoResponse($status_code, $response) {
        $app = \Slim\Slim::getInstance();
        $app->status($status_code);
        
        $app->contentType('application/json');
        
        echo json_encode($response);
    }
    
    /**
     * Criação de uma nova tarefa
     * method: POST
     * url: /tasks/
     */
    $app->post('/tasks', 'authenticate', function() use ($app) {
        verifyRequiredParams(array('task', 'taskDesc', 'priority'));

        $response = array();
        $task = $app->request->post('task');
        $taskDesc = $app->request->post('taskDesc');
        $priority = $app->request->post('priority');

        $db = new DbHandler();

        $task_id = $db->createTask($task, $taskDesc, $priority);

        if ($task_id != NULL) {
            $response["error"] = false;
            $response["message"] = "Tarefa criada com sucesso!";
            $response["task_id"] = $task_id;
        } 
        else {
            $response["error"] = true;
            $response["message"] = "Erro! Falha em cirar a tarefa. Tente novamente.";
        }
        echoResponse(201, $response);
    });    
    
    /**
     * Listando todas as tarefas
     * method: GET
     * url: /tasks          
     */
    $app->get('/tasks', 'authenticate', function() {
        $response = array();
        $db = new DbHandler();

        $result = $db->getAllTasks();

        $response["error"] = false;
        $response["tasks"] = array();

        // Feito um loop para popular o array $response['tasks'] com todas as tarefas
        while ($task = $result->fetch_assoc()) {
            $tmp = array();
            $tmp["id"]        = $task["id"];
            $tmp["task"]      = $task["task"];
            $tmp["taskDesc"]  = $task["taskDesc"];
            $tmp["status"]    = $task["status"];
            $tmp["priority"]  = $task["priority"];
            $tmp["createdAt"] = $task["created_at"];
            array_push($response["tasks"], $tmp);
        }

        echoResponse(200, $response);
    });
    
    /**
     * Listar uma única tarefa
     * Método GET
     * url /tasks/:id
     * Irá retornar 404 se a tarefa não existir
     */
    $app->get('/tasks/:id', 'authenticate', function($task_id) {
            $response = array();
            $db = new DbHandler();
 
            // Busca a tarefa
            $result = $db->getTask($task_id);
 
            if ($result != NULL) {
                $response["error"]     = false;
                $response["id"]        = $result["id"];
                $response["task"]      = $result["task"];
                $response["taskDesc"]  = $result["taskDesc"];
                $response["status"]    = $result["status"];
                $response["priority"]  = $result["priority"];
                $response["createdAt"] = $result["created_at"];
                echoResponse(200, $response);
            } else {
                $response["error"]     = true;
                $response["message"]   = "Não existe tarefa para o ID #".$task_id;
                echoResponse(404, $response);
            }
        });
 
    /**
     * Atualizando tarefa
     * method: PUT
     * url: /tasks/:id
     */
    $app->put('/tasks/:id', 'authenticate', function($task_id) use($app) {
        verifyRequiredParams(array('task_id', 'task', 'taskDesc', 'priority', 'status'));

        $request_params = array();
        
        parse_str($app->request()->getBody(), $request_params);            
        $task       = $request_params['task'];
        $taskDesc   = $request_params['taskDesc'];
        $priority   = $request_params['priority'];
        $status     = $request_params['status'];

        echo $task;

        $db = new DbHandler();
        $response = array();

        $result = $db->updateTask($task_id, $task, $taskDesc, $priority, $status);
        if ($result) {
            $response["error"] = false;
            $response["message"] = "Tarefa atualizada com sucesso!";
        } else {
            $response["error"] = true;
            $response["message"] = "Tarefa não pode ser atualizada. Tente novamente.";
        }
        echoResponse(200, $response);
    });
     
     
    /**
     * Deletar uma tarefa
     * method: DELETE
     * url: /tasks
     */
    $app->delete('/tasks/:id', 'authenticate', function($task_id) use($app) {
        $db = new DbHandler();
        $response = array();
        $result = $db->deleteTask($task_id);
        if ($result) {
            $response["error"] = false;
            $response["message"] = "Tarefa deletada com sucesso!";
        } else {
            $response["error"] = true;
            $response["message"] = "Tarefa não pode ser deletada. Tente novamente.";
        }
        echoResponse(200, $response);
    });
    
    $app->run();
?>