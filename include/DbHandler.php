<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /**
     * Criação de nova tarefa
     * @param String $task Título da tarefa
     * @param String $taskDesc Descrição da tarefa
     * @param String $priority Prioridade da tarefa
     */
    public function createTask($task, $taskDesc, $priority) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task, description, priority) VALUES(?)");
        $stmt->bind_param("sss", $task, $taskDesc, $priority);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            $new_task_id = $this->conn->insert_id;
            return $new_task_id;
        } else {
            // falha ao criar
            return NULL;
        }
    }

    /**
     * Exibição de uma tarefa
     * @param String $task_id: id da tarefa
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.* from tasks t WHERE t.id = ? AND t.status = 1");
        $stmt->bind_param("ii", $task_id);
        if ($stmt->execute()) {
            $task = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $task;
        } else {
            return NULL;
        }
    }

    /**
     * Exibição de todas as tarefas
     */
    public function getAllTasks() {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t WHERE t.status = 1 ORDER BY priority ASC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        
        return $tasks;
    }

    /**
     * Atualizar tarefa
     * @param String $task_id: id da tarefa
     * @param String $task: Título da tarefa
     * @param String $taskDesc: Descrição da tarefa
     * @param String $priority: Prioridade da tarefa
     * @param String $status: Status da tarefa
     */
    public function updateTask($task_id, $task, $taskDesc, $priority, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t SET t.title = ?, t.description = ?, t.priority = ?, t.status = ? WHERE t.id = ?");
        $stmt->bind_param("ssiii", $task, $taskDesc, $priority, $status, $task_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        
        return $num_affected_rows > 0;
    }

    /**
     * Exclusão de tarefa
     * @param String $task_id id da tarefa a ser deletada
     */
    public function deleteTask($task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t WHERE t.id = ?");
        $stmt->bind_param("i", $task_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        
        return $num_affected_rows > 0;
    }

}

?>