<?php

$x = 1;
$max = 100;

echo utf8_decode('<strong>Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
"Fizz" em vez do número e para múltiplos de 5 imprima "Buzz". Para números múltiplos 
de ambos (3 e 5), imprima "FizzBuzz".<strong><br><br>');

// Apenas para facilitar a leitura
echo "<table><tr>";

while($x <= $max){
    $y = $x;
    
    if((($y % 3) == 0) && (($y % 5) == 0))
        $y = '<strong style="color: purple">FizzBuzz</strong>';
    else if(($y % 3) == 0)
        $y = '<strong style="color: blue">Fizz</strong>';
    else if(($y % 5) == 0)
        $y = '<strong style="color: red">Buzz</strong>';
    
    echo '<td style="width:50px; text-align:center; font-weight:bold;">'. $y . "<td>";
    
    // Apenas para facilitar a leitura
    if(($x % 10) == 0)
        echo "</tr><tr>";
    
    $x++;
}

// Apenas para facilitar a leitura
echo "</tr></table>";
?>